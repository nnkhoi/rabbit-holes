package com.kms.challenges.rbh.controller;

import com.kms.challenges.rbh.domain.Comment;
import com.kms.challenges.rbh.model.SimpleUserDetails;
import com.kms.challenges.rbh.repository.CommentRepository;
import com.kms.challenges.rbh.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

/**
 * @author tkhuu.
 */
@Controller
public class GuestBookController {
    private static final String VIEW_GUESTBOOK = "guestbook";
    private static final String REDIRECT_GUESTBOOK = "redirect:/guestbook";
    private static final Logger LOGGER = LoggerFactory.getLogger(GuestBookController.class.getCanonicalName());
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "guestbook", method = RequestMethod.GET)
    public String list(@CookieValue(value = "admin", required = false, defaultValue = "false") Boolean admin, Model
            model) {
        LOGGER.debug(admin.toString());
        model.addAttribute("isAdmin", true);
        List<Comment> commentList = commentRepository.findAll();
        model.addAttribute("commentList", commentList);
        Comment comment = new Comment();
        Long user_id = ((SimpleUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getId();
        comment.setUser(userRepository.findOne(user_id));
        model.addAttribute("comment", comment);
        return VIEW_GUESTBOOK;
    }

    @RequestMapping(value = "guestbook", method = RequestMethod.POST)
    public String addComment(@Valid Comment comment, BindingResult result) {
        if (result.hasErrors()) {
            return VIEW_GUESTBOOK;
        }

        commentRepository.saveAndFlush(comment);
        return REDIRECT_GUESTBOOK;
    }

    @RequestMapping(value = "guestbook/delete", method = RequestMethod.GET)
    public String deleteComment(@RequestParam("commentId") Long id) {
        commentRepository.delete(id);

        return REDIRECT_GUESTBOOK;
    }

}
