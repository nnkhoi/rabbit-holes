package com.kms.challenges.rbh.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * @author tkhuu.
 */
@Entity
public class Comment extends BaseEntity<Long> {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Column(length = 500)
    private String comment;
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
